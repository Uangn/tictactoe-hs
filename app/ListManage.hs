module ListManage (modify) where

modify :: (a -> a) -> Int -> [a] -> [a]
modify f s ls = xs++[f y]++ys
    where (xs,y:ys) = splitAt s ls
