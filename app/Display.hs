module Display 
    ( printBoard
    , printGame
    ) where

---- Imports
-- Extern
import Data.List (intercalate, zipWith)
import Data.Vector (toList)
-- Intern
import Board

clearScreen = "\ESC[2J"

printGame :: Game -> IO()
printGame = printBoard . gameBoard

printBoard :: Board -> IO()
printBoard = putStrLn . showBoard

showBoard :: Board -> String
showBoard b = clearScreen ++ board ++ "  1 2 3\n"
    where
        board = intercalate "  -----\n"
              $ reverse 
              $ zipWith
                    (\i r -> show i ++ " " ++ showRow r)
                    [1,2,3] 
                    (toList b)

        showRow = (++ "\n") 
                . intercalate "|" 
                . map showCell 
                . toList

showCell :: Cell -> String
showCell (Just X) = "X"
showCell (Just O) = "O"
showCell Nothing  = " "
