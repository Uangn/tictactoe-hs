module Main where

-- Extern
import Text.Read (readMaybe)
-- Intern
import Board
import Display

main :: IO()
main = do
    gamemode <- putStrLn "What gamemode? \n1. Single Player \n2. Two Player" >> getLine
    case read gamemode of
        2 -> printGame initialGame >> playGame2 initialGame
        _ -> putStrLn "Invalid gamemode" >> main




playGame2 :: Game -> IO ()
playGame2 game = do
    line <- getLine
    -- Try making a move
    maybe retry next $ do
        (i, j) <- readMaybe line
        makeMove (i-1, j-1) game
    where
        retry = putStrLn "Invalid move. Please input a valid move." >> playGame2 game
        
        -- Move success
        next currGame = do
            printGame currGame
            case getGameState currGame of
                Won a   -> putStrLn $ "Player " ++ show a ++ " won!"
                Draw    -> putStrLn   "It's a draw!"
                Running -> playGame2 currGame

