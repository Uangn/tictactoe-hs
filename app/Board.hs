module Board
        ( Player (..)
        , Cell
        , Board
        , Game (..)
        , GameState (..)
        , initialGame
        , makeMove
        , getGameState
        ) where

---- Imports
-- Extern
import Data.List.Split (chunksOf)
import Data.Vector ((!), (//), Vector)
import qualified Data.Vector as V
-- Intern
import ListManage (modify)

---- Data
data Player = X | O
    deriving (Eq, Show)

type Cell = Maybe Player

type Board = Vector (Vector Cell)

-- X is positive
-- O is negative
-- +/- whenever played in rows,cols,diags
-- [r1,r2,r3, c1,c2,c3, tl-br,tr-bl]
type ZoneCounts = [Int]

type Move = (Int, Int)

data Game = Game
            { gameBoard :: Board
            , currentPlayer :: Player
            , zoneCounts :: ZoneCounts
            , turn :: Int
            } deriving (Show)

data GameState = Won Player | Draw | Running




---- Initial States
initialGame :: Game
initialGame = Game
                { gameBoard = initialBoard
                , currentPlayer = X
                , zoneCounts = initialZoneCounts
                , turn = 0
                }

initialBoard :: Board
initialBoard = V.replicate 3 $ V.replicate 3 Nothing

initialZoneCounts :: ZoneCounts
initialZoneCounts = replicate 8 0

---- Logic

-- Using the flags for the wins
-- Returns the potential player that won
-- (2n)
getGameState :: Game -> GameState
getGameState (Game _ _ z t)
    | 3 `elem` z     = Won X
    | (-3) `elem` z  = Won O
    | t == 9         = Draw
    | otherwise      = Running


-- Gives the other Player Id
oppositePlayer :: Player -> Player
oppositePlayer X = O
oppositePlayer O = X

-- Returns whether the cell is empty or not
validMove :: Move -> Board -> Bool
validMove (x,y) b = validRanges && Nothing == (b ! x ! y)
    where
        validRange a = 0 <= a && a <= 2
        validRanges = validRange x && validRange y


-- returns the game after a move, changes the player, and updates the zone flags
-- or fails if the move spot is taken
makeMove :: (Int, Int) -> Game -> Maybe Game
makeMove m (Game b p z t)
    | validMove m b = Just $ Game
                                { gameBoard = modifyBoard m p b
                                , currentPlayer = oppositePlayer p
                                , zoneCounts = modifyZoneCounts m p z
                                , turn = t+1
                                }
    | otherwise = Nothing



-- Places a player move
-- Not recommended to use without zoneCounts
-- Use makeMove instead
modifyBoard :: (Int, Int) -> Player -> Board -> Board
modifyBoard (x,y) p b = b // [(x, modRow)]
    where modRow = b ! x // [(y, Just p)]


-- Updates the zone flags based on moves
modifyZoneCounts :: (Int, Int) -> Player -> ZoneCounts -> ZoneCounts
modifyZoneCounts (x,y) p z = newRows ++ newCols ++ newDiags
    where
        (rows, cols, diags) = (\[a,b,c] -> (a,b,c)) $ chunksOf 3 z
        newRows = modify (+playerMove) y rows
        newCols = modify (+playerMove) x cols
        newDiags
            | x==1 && y==1 = map (+playerMove) diags
            | x==y         = modify (+playerMove) 0 diags
            | x+y == 2     = modify (+playerMove) 1 diags
            | otherwise    = diags

        playerMove
            | p == X = 1
            | p == O = -1
